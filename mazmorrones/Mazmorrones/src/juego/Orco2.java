package juego;


/**
 * Clase que define todas las funciones específicas del Enemigo de classe Orco2
 * @author Rafael González
 *
 */
public class Orco2 extends Enemigo
{
	/**
	 * Variable que define el el Path de origen de Orco2
	 */
	String origen;
	/**
	 * Boolean qeu determina si un Objeto Orco2 esta en stun
	 */
	public boolean stun = false;
	public int timeStun = 65;

	public Orco2(String nombre, int x1, int y1, int x2, int y2, String path)
	{
		super(nombre, x1, y1, x2, y2, path);
		this.origen=path;
	}
	
	/**
	 * Función que determina cuando Objeto de clase Orco2, puede atacar
	 */	
	public void e2Ataca() 
	{
		if (!stun) 
		{
			if (this.collidesWith(Warro.getWarro())) 
			{				
					if (this.origen.equals("enemigo2.gif"))
					{
						changeImage("enemigo2Ataque.gif");
					} 
					
					else if (this.origen.equals("enemigo2Der.gif")) 
					{
						changeImage("enemigo2AtaqueDer.gif");
					}
				
				if (Warro.getWarro().inmune >= 65)
				{
					if (Main.w.getPressedKeys().contains('n')) 
					{
						//Warro.getWarro().changeImage("gBloqueoAlto.gif");
					}
					else 
					{
						Warro.getWarro().numGolpes++;
						Warro.getWarro().inmune = 0;
					}
				} 			
			}
			else
			{
				changeImage(this.origen);
			}
		}
		else
		{
			if (this.origen.equals("enemigo2.gif"))
			{
				changeImage("enemigo2Stun.gif");
			}
			else
			{
				changeImage("enemigo2StunDer.gif");
			}
		}
	}
	
}
