package juego;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Clase que define todas las funciones específicas de la clase Mapa
 * @author Rafael González
 *
 */
public class Mapa implements Serializable
{
	
	public Mapa(String name)
	{
		
	}
	/**
	 * Array que contiene los Obstáculos
	 */
	ArrayList<Obstaculo> obsList = new ArrayList<>();
	/**
	 * Array que contiene los Enemigos
	 */
	ArrayList<Enemigo> enemList = new ArrayList<>();
	/**
	 * Array que contiene los Objetos
	 */
	ArrayList<Objeto> objList = new ArrayList<>();
	
	/**
	 * Función que borra las arrays de: Obstaculos, Enemigos y Objetos
	 */
	 public void borrarMapa()
	 {
		 obsList.clear();
		 enemList.clear();
		 objList.clear();
	 }
	//sala1
	 /**
	  * Objetos que definen los obstáculos en las salas
	  */
	 Obstaculo mR = new Obstaculo("muro", 1346, 300, 1442, 778, "");
	 Obstaculo mL = new Obstaculo("muro", 450, 300, 558, 750, "");
	 Obstaculo mI1 = new Obstaculo("muro", 558, 700, 780, 750, "");
	 Obstaculo mI2 = new Obstaculo("muro", 1112, 700, 1436, 750, "");
	 Obstaculo mesa1 = new Obstaculo("mesa", 518, 154, 806, 374, "");
	 Obstaculo mesa2 = new Obstaculo("mesa", 1086, 154, 1376, 374, "");
	 //sala2
	 Obstaculo mCentral = new Obstaculo("muro", 640, 448, 1216, 750, "");
	 Obstaculo mSuperior1 = new Obstaculo("muro", 240, 170, 836, 200, "");
	 Obstaculo mSuperior2= new Obstaculo("muro", 1100, 170, 1584, 200, "");
	 Obstaculo mDerecho = new Obstaculo("mesa", 1504, 200, 1584, 802, "");
	 Obstaculo mIzquierdo = new Obstaculo("mesa", 180, 200, 344, 802, "");
	 /**
	  * Objetos que definen los Enemigos en las salas
	  */
	 Orco1 eIzq = new Orco1("o1", 380, 266, 540, 426, "e2normalDer.gif");
	 Orco1 eDer = new Orco1("o2", 1297, 266, 1457, 426, "e2normal2.gif");
	 
	 Orco2 i = new Orco2("o", 380, 266, 540, 426, "enemigo2Der.gif");
	 //sala3
	 Obstaculo central1 = new Obstaculo("centro1", 936, 254, 996, 286, "");
	 Obstaculo central2 = new Obstaculo("centro2", 936, 602, 996, 638, "");
	 Obstaculo mSup1 = new Obstaculo("muroSuperiorIzq", 500, 3, 852, 18, "");
	 Obstaculo mSup2= new Obstaculo("muroSuperiorDer", 1060, 3, 1432, 18, "");
	 Obstaculo mDerech = new Obstaculo("derecha", 1320, 20, 1432, 890, "");
	 Obstaculo mIzquierd = new Obstaculo("izquierda", 500, 20, 600, 890, "");
	 Obstaculo mInf1 = new Obstaculo("muroInferiorIzq", 500, 866, 832, 998, "");
	 Obstaculo mInf2= new Obstaculo("muroInferiorDer", 1070, 866, 1432, 998, "");
	 
	 Orco1 izq = new Orco1("arri", 656, 123, 816, 283, "e2normalDer.gif");
	 Orco1 der = new Orco1("med", 1088, 407, 1248, 567, "e2normal2.gif");
	 Orco1 izq2 = new Orco1("abaj", 660, 675, 820, 835, "e2normalDer.gif");

	 //sala4
	 Obstaculo ataudIz = new Obstaculo("ataudIzquierda", 536, 586, 762, 894, "");
	 Obstaculo ataudDe = new Obstaculo("ataudDerecha", 1212, 586, 1472, 894, "");
	 Obstaculo muritoIzq = new Obstaculo("muritoSuperiorIzq", 536, 302, 832, 303, "");
	 Obstaculo muritoDer = new Obstaculo("muritoSuperiorDer", 1092, 302, 1472, 303, "");
	 Obstaculo murito2Izq = new Obstaculo("muritoInferiorIzq", 536, 574, 832, 575, "");
	 Obstaculo murito2Der = new Obstaculo("muritoInferiorDer", 1092, 574, 1472, 575, "");
	 Obstaculo mSupIz = new Obstaculo("muroSuperiorIzq", 500, 3, 852, 14, "");
	 Obstaculo mSupDe= new Obstaculo("muroSuperiorDer", 1044, 3, 1432, 14, "");
	 Obstaculo mD = new Obstaculo("derecha", 1324, 20, 1432, 970, "");
	 Obstaculo mI = new Obstaculo("izquierda", 500, 20, 604, 970, "");
	 Obstaculo mInfI = new Obstaculo("muroInferiorIzq", 500, 858, 852, 970, "");
	 Obstaculo mInfD= new Obstaculo("muroInferiorDer", 1076, 858, 1432, 970, "");
	 /**
	  * Objetos que definen los items en las salas
	  */
	 Objeto cor = new Objeto("curacion", 670, 400, 760, 500, "cura3.gif");
	 
	 Orco1 eIzq1 = new Orco1("izq1", 692, 127, 852, 287, "e2normalDer.gif");
	 Orco1 eDer1 = new Orco1("der1", 1056, 127, 1216, 287, "e2normal2.gif");
	 Orco1 eIzq2 = new Orco1("izq2", 784, 387, 944, 547, "e2normalDer.gif");
	 Orco1 eDer2 = new Orco1("der2", 1112, 387, 1272, 547, "e2normal2.gif");

	 //sala5
	 Obstaculo central = new Obstaculo("muroCentral", 708, 290, 1288, 438, "");
	 Obstaculo inferior1 = new Obstaculo("muroInferiorIzq", 308, 710, 792, 760, "");
	 Obstaculo inferior2= new Obstaculo("muroInferiorDer", 1136, 710, 1700, 760, "");
	 Obstaculo izquierdo = new Obstaculo("muroIzquierdo", 312, 290, 416, 760, "");
	 Obstaculo derecho = new Obstaculo("muroDerecho", 1572, 290, 1700, 760, "");
	 //sala6
	 Obstaculo supI = new Obstaculo("superiorIzq", 460, -70, 860, -50, "");
	 Obstaculo supD = new Obstaculo("superiorDer", 1060, -70, 1480, -50, "");
	 Obstaculo mIz= new Obstaculo("muroIzquierdo", 440, 60, 570, 560, "");
	 Obstaculo mDe = new Obstaculo("muroDerecho", 1330, 60, 1430, 560, "");
	 Obstaculo lapidaI = new Obstaculo("lapidaIzq", 440, 280, 710, 760, "");
	 Obstaculo lapidaD = new Obstaculo("lapidaDer", 1200, 280, 1430, 760, "");
	 Obstaculo altar = new Obstaculo("altar", 710, 780, 1200, 1000, "");
	 
	 Orco2 bos1 = new Orco2("jefeIzq", 727, 278, 887, 438, "enemigo2Der.gif");
	 Orco2 bos2 = new Orco2("jefeDer", 1011, 278, 1171, 438, "enemigo2.gif");

	 /**
	  * Función que decide que enemigos van en cada sala
	  * @param sala y ronda actual del jugador declarado en el Main
	  * @return ArrayList de Enemigos
	  */
	 public ArrayList<Enemigo> crearEnemigos(int sala)
	 {
		 enemList.clear();
		 
//			 if (ronda == 1)
//			 {
				switch (sala) 
				{
				case 1:

					break;
				case 2:
					enemList.add(eIzq);
					enemList.add(eDer);
					break;
				case 3:
					enemList.add(izq);
					enemList.add(der);
					enemList.add(izq2);
					break;
				case 4:
					enemList.add(eIzq1);
					enemList.add(eDer1);
					enemList.add(eIzq2);
					enemList.add(eDer2);
					break;
				case 5:

					break;
				case 6:
					enemList.add(bos1);
					enemList.add(bos2);
					break;
				default:
					break;
				}
//			}
//			else if (ronda == 2)
//			{
//				enemList.clear();
//				switch (sala)
//			 	{
//			 	case 1:
//			
//			 		break;
//			 	case 2:			 
//			 		enemList.add(i);
//			 		break;
//			 	case 3:
//				
//			 		break;
//			 	case 4:
//								
//			 		break;
//			 	case 5:
//			
//			 		break;
//			 	case 6:
//				
//			 		break;
//			 	default:
//			 		break;
//			 	}
//			}
	 	
		 return enemList;
	 }
	 /**
	  * Función que decide que objetos van en cada sala
	  * @param sala actual del jugador declarado en el Main
	  * @return ArrayList de Objetos
	  */
	 public ArrayList<Objeto> crearObjetos(int sala)
	 {
		 objList.clear();
		 switch (sala)
		 {
		case 1:
			
			break;
		case 2:
		
			break;
		case 3:
			
			break;
		case 4:
			objList.add(cor);				
			break;
		case 5:
			
			break;
		case 6:
		
			break;
		default:
			break;
		} 
		 return objList;
	 }
	 /**
	  * Función que decide que obstáculos hay en cada sala
	  * @param sala actual del jugador declarado en el Main
	  * @return ArrayList de Obstáculos
	  */
	public ArrayList<Obstaculo> crearObstaculos(int sala) 
	 {
		 obsList.clear();
		 switch (sala)
		 {
		case 1:
			 obsList.clear();			 
			 obsList.add(mR);
			 obsList.add(mL);
			 obsList.add(mI1);
			 obsList.add(mI2);
			 obsList.add(mesa1);
			 obsList.add(mesa2);
			break;
		case 2:
			 obsList.add(mCentral);
			 obsList.add(mSuperior1);
			 obsList.add(mSuperior2);
			 obsList.add(mDerecho);
			 obsList.add(mIzquierdo);
			break;
		case 3:
			 obsList.add(central1);
			 obsList.add(central2);
			 obsList.add(mSup1);
			 obsList.add(mSup2);
			 obsList.add(mDerech);
			 obsList.add(mIzquierd);
			 obsList.add(mInf1);
			 obsList.add(mInf2);
			break;
		case 4:
			 obsList.add(ataudIz);
			 obsList.add(ataudDe);
			 obsList.add(muritoIzq);
			 obsList.add(muritoDer);
			 obsList.add(murito2Izq);
			 obsList.add(murito2Der);
			 obsList.add(mSupIz);
			 obsList.add(mSupDe);
			 obsList.add(mD);
			 obsList.add(mI);
			 obsList.add(mInfI);
			 obsList.add(mInfD);						
			break;
		case 5:
			 obsList.add(central);
			 obsList.add(inferior1);
			 obsList.add(inferior2);
			 obsList.add(izquierdo);
			 obsList.add(derecho);
			break;
		case 6:
			 obsList.add(supI);
			 obsList.add(supD);
			 obsList.add(mIz);
			 obsList.add(mDe);
			 obsList.add(lapidaI);
			 obsList.add(lapidaD);
			 obsList.add(altar);
			break;
		default:
			break;
		} 
		 return obsList;
	 }
}
