package juego;

import java.util.Random;

public abstract class Enemigo extends Sprite
{
	 int vida;
	 int defensa;
	 int fuerza;
	 int velAtaque;
//	 String clase;
	 String nombre;
	 boolean estaVivo;
	 int cooldown=25;
	 static Random r = new Random();
	 int ataque = r.nextInt(50);

	 
	public Enemigo(String nombre, int x1, int y1, int x2,int y2, String path) 
	{
		super(nombre, x1, y1, x2, y2, path);
		this.vida = 500;
		this.defensa = 100;
		this.fuerza = 100;
		this.velAtaque = 4;
//		this.clase = clase;
		this.nombre = nombre;
		this.estaVivo = true;
//		this.x1 = r.nextInt(886)+188 ;
//		this.y1 = r.nextInt(750)+60;
//		this.x2 = x1 +170 ;
//		this.y2 = y1 +170;
	}
	
	public void update() throws InterruptedException 
	{
		if(cooldown<25)cooldown++;
		if (!delete)
		{
			if (estaVivo == false) 
			{
				changeImage("eMuere.gif");
				Thread.sleep(1000);
				//changeImage("");
				delete();
			} 
		}
	}
}
