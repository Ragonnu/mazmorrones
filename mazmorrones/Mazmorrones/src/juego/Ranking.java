package juego;

import java.util.Date;


/**
 * Clase que define todas las funciones específicas de la clase Ranking
 * @author Rafael González
 *
 */
public class Ranking implements Comparable
{
	/**
	 * Variable que define el nombre del jugador en el ranking
	 */
	String nombre;
	/**
	 * Variable que define los puntos del jugador en el ranking
	 */
	int puntos;
	String fecha;
	
	public Ranking(String nombre, int puntos) 
	{
		super();
		this.nombre = nombre;
		this.puntos = puntos;
		this.fecha = new Date().toString();
	}
	
	/**
	 * Función que compara los puntos de un Objeto Ranking
	 */	
	@Override
	public int compareTo(Object o) 
	{
		Ranking r = (Ranking) o;
		if(r.puntos < this.puntos)
		{
			return -1;
		}
		else if(r.puntos == this.puntos)
		{
			return 0;
		}
		else 
		{
			return 1;
		}		
	}
}
