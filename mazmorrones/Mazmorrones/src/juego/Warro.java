package juego;

import java.io.Serializable;
import java.util.ArrayList;




/**
 * Clase que define todas las funciones específicas del heroe de classe guerrero
 * @author Rafael González
 *
 */
public class Warro extends Heroe
{
	boolean bloqueando = false;
	public static Warro w = null;
	int numGolpes = 0;
	int inmune = 65;
	
	private Warro()
	{
		super(500, 100, 50, 10, 10, 1, "Warro", "nombre", true, "gNormal.gif");
	}
	
  
	/**
	 * Función que crea un Objeto Warro, si ya está creado, te lo retorna
	 * @return objeto Warro 
	 */
	public static Warro getWarro() 
	{
		if(w==null) 
		{
			w= new Warro();
			return w;
		}
		else 
		{
			return w;
		}		
	}
	
	
	/**
	 * Función que actualiza el estado del personaje
	 */
	public void update() 
	{
		if(cooldown<25)cooldown++;
		inmune++;
//		System.out.println(this.direccion);
//		if(this.bloqueando) {
//			if(this.direccion=='a') 
//			{
//				this.changeImage("gNormalIzq.gif");
//			}
//			else 
//			{
//				this.changeImage("gNormal.gif");
//			}
//		}
		posicionReposo();
		if(this.collidesWith(Main.salas.get(3).objList.get(0)))
		{
			if(numGolpes == 2||numGolpes == 1)
			{
				numGolpes--;
				Main.salas.get(3).objList.get(0).delete();
			}
		}
	}

	/**
	 * Busca si hay un enemigo enfrente y lo mata
	 * @param f El Field de juego declarado en el Main
	 */
	public void golpeRapido(Field f)
	{
		Main.w.playSFX("golpe.wav");
		if (!bloqueando) 
		{
			if (this.direccion == 'a')
			{
				this.changeImage("gGolpeRapidoIzq.gif");
			} 
			else
			{
				this.changeImage("gGolpeRapido.gif");
			}
			
			ArrayList<Sprite> listCol = collidesWithField(f);
			if (listCol != null)
			{
				for (Sprite s : listCol)
				{
					if (s instanceof Orco1)
					{
						s.delete();
						this.score += 100;
					}
				}
			} 
		}
	}
	/**
	 * Busca si hay un enemigo enfrente y lo mata
	 * @param f El Field de juego declarado en el Main
	 */
	public void estocada(Field f)
	{
		Main.w.playSFX("golpe.wav");
		if (!bloqueando) 
		{
			if (this.direccion == 'a')
			{
				this.changeImage("gEstocadaIzq.gif");
			} 
			else
			{
				this.changeImage("gEstocada.gif");
			}
			ArrayList<Sprite> listCol = collidesWithField(f);
			if (listCol != null)
			{
				for (Sprite s : listCol) 
				{
					if (s instanceof Orco1)
					{
						s.delete();
						this.score += 100;
					}
				}
			} 
		}
	}
	/**
	 * Busca si hay un enemigo enfrente y lo aturde
	 * @param f El Field de juego declarado en el Main
	 */
	public void golpeEscudo(Field f)
	{
		Main.w.playSFX("golpeEscudo.wav");
		if(this.direccion=='a') 
		{
			this.changeImage("gGolpeEscudoIzq.gif");
		}
		else 
		{
			this.changeImage("gGolpeEscudo.gif");
		}
		
		ArrayList<Sprite> listCol = collidesWithField(f);
		if (listCol != null)
		{
			for(Sprite s : listCol) 
			{
				if(s instanceof Orco1) 
				{
					((Orco1) s).stun = true;
				}
				else if(s instanceof Orco2) 
				{
					((Orco2) s).stun = true;
				}
			}
		}
	}
	/**
	 * Función que pone al jugador en guardia
	 * Si un enemigo le ataca, el jugador bloquea sus ataques
	 * @param f El Field de juego declarado en el Main
	 */
	public void cubrir(Field f)
	{
		//bloqueando = true;
		if(this.direccion=='a') 
		{
			this.changeImage("gGuardiaIzq.gif");
		}
		else 
		{
			this.changeImage("gGuardia.gif");
		}
		ArrayList<Sprite> listCol = collidesWithField(f);
		if (listCol != null)
		{
			for(Sprite s : listCol) 
			{
				if(s instanceof Orco1) 
				{
					if (!((Orco1) s).stun) 
					{
						if (this.direccion == 'a')
						{
							this.changeImage("gBloqueoAltoIzq.gif");
						} 
						else 
						{
							this.changeImage("gBloqueoAlto.gif");
						} 
					}
					
				}
			}
		}
	}
	/**
	 * Busca si hay un enemigo enfrente y lo mata
	 * @param f El Field de juego declarado en el Main
	 */
	public void golpe(Field f)
	{
		Main.w.playSFX("golpe.wav");
		if (!bloqueando)
		{
			if (this.direccion == 'a')
			{
				this.changeImage("gAtaqueIzq.gif");
			} 
			else 
			{
				this.changeImage("gAtaque.gif");
			}
			ArrayList<Sprite> listCol = collidesWithField(f);
			if (listCol != null) 
			{
				for (Sprite s : listCol)
				{
					if (s instanceof Orco1) 
					{
						s.delete();
						this.score += 100;
					}
				}
			} 
		}
	}
	/**
	 * Busca si hay un enemigo enfrente y lo mata
	 * Solo se puede usar si tienes su PoweUp
	 * @param f El Field de juego declarado en el Main
	 */
	public void golpeEspecial(Field f)
	{
		Main.w.playSFX("golpe.wav");
		if (!bloqueando)
		{
			if (this.direccion == 'a')
			{
				this.changeImage("gGolpeEspecialIzq.gif");
			} 
			else 
			{
				this.changeImage("gGolpeEspecial.gif");
			}
			ArrayList<Sprite> listCol = collidesWithField(f);
			if (listCol != null) 
			{
				for (Sprite s : listCol)
				{
					if (s instanceof Orco1) 
					{
						s.delete();
						this.score += 100;
					}
				}
			} 
		}
	}
	
	
	/**
	 * Función muestra la vida del Heroe
	 * @throws InterruptedException
	 */
	void vida() throws InterruptedException
	{
		Mapa m = new Mapa("");
		
		if(numGolpes == 0)
		{
			Main.corazones.changeImage("3cor.png");
		}
		if(numGolpes == 1)
		{
			Main.corazones.changeImage("2cor.png");
		}
		else if (numGolpes == 2)
		{
			Main.corazones.changeImage("1cor.png");
		}
		else if (numGolpes == 3)
		{
			Main.corazones.delete();
			
			delete();
//			Main.borrarSalas();
			
			Main.f.background="gMuriendo.gif";
			Thread.sleep(2100);
			Main.f.background="fondoFinal1.png";
			Main.finPartida = true;
		}
	}
	/**
	 * Función que pone al Heroe en posición de reposo
	 */
	public void posicionReposo()
	{
		if (this.iddle)
		{
			if (this.direccion == 'a')
			{
				this.changeImage("gNormalIzq.gif");
			} 
			else 
			{
				this.changeImage("gNormal.gif");
			}
		}
	}
	
}
