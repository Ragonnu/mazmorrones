package juego;

/**
 * Clase que define todas las funciones específicas de la clase Texto
 * @author Rafael González
 *
 */
public class Texto extends Sprite
{

		public Texto(String name, int x1, int y1, int x2, int y2, String path) 
		{
			super(name, x1, y1, x2, y2, path);
			this.text=true;
		
		}

}

