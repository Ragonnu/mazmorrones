package juego;

/**
 * Clase que define todas las funciones específicas del Enemigo de classe Orco1
 * @author Rafael González
 *
 */
public class Orco1 extends Enemigo
{
	/**
	 * Variable que define el el Path de origen de Orco1
	 */
	String origen;
	/**
	 * Boolean qeu determina si un Objeto Orco1 esta en stun
	 */
	public boolean stun = false;
	public int timeStun = 65;
	
	public Orco1(String name, int x1, int y1, int x2, int y2, String path)
	{
		super(name, x1, y1, x2, y2, path);
		this.origen=path;
	}

	/**
	 * Función que determina cuando Objeto de clase Orco1, puede atacar
	 */
	public void eAtaca() 
	{
		if (!stun) 
		{
			if (this.collidesWith(Warro.getWarro())) 
			{				
					if (this.origen.equals("e2normal2.gif"))
					{
						changeImage("e2Ataque3.gif");
					} 
					
					else if (this.origen.equals("e2normalDer.gif")) 
					{
						changeImage("e2Ataque3Der.gif");
					}
				
				if (Warro.getWarro().inmune >= 65)
				{
					if (Main.w.getPressedKeys().contains('n')) 
					{
						//Warro.getWarro().changeImage("gBloqueoAlto.gif");
					}
					else 
					{
						Warro.getWarro().numGolpes++;
						Warro.getWarro().inmune = 0;
					}
				} 			
			}
			else
			{
				changeImage(this.origen);
			}
		}
		else
		{
			if (this.origen.equals("e2normal2.gif"))
			{
				changeImage("e2Atacado.gif");
			}
			else
			{
				changeImage("e2AtacadoDer.gif");
			}
		}
	}
}
