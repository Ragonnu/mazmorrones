package juego;
import java.io.IOException;
import java.util.Random;

/**
 * Clase que define todas las funciones específicas de la clase Heroe
 * @author Rafael González
 *
 */
public abstract class Heroe extends Sprite
{
	 public int score = 0;
	 int defensa;
	 int poder;
	 int fuerza;
	 int velAtaque;
	 int inteligencia;
	 String clase;
	 String nombre;
	 boolean estaVivo;
	 char direccion = 'd';
	 int cooldown=25;
	 Random r = new Random();
	 int ataque = r.nextInt(50);
	 int vida;
	 int velMov = 4;
	 int velColision = 2;
	 boolean iddle = true;
	
	 
		public Heroe(int vida, int defensa, int poder,int fuerza,int velAtaque,int inteligencia, String clase, String nombre, boolean estaVivo, String path) 
		{
			super(nombre, 866, 134, 1026, 294, path);
			this.vida = vida;
			this.defensa = defensa;
			this.poder = poder;
			this.fuerza = fuerza;
			this.velAtaque = velAtaque;
			this.inteligencia = inteligencia;
			this.clase = clase;
			this.nombre = nombre;
			this.estaVivo = estaVivo;
		}
	/**
	 * Función que saca por consola los atributos del heroe
	 */	
	@Override
	public String toString() 
	{
		return "Heroe [name=" + name + ", x1=" + x1 + ", y1=" + y1 + ", x2=" + x2 + ", y2=" + y2 + "]";
	}

	/**
	 * Función que mueve al heroe a la izquierda
	 * @param f El Field de juego declarado en el Main
	 */
	public void moveIzq(Field f) 
	{
		this.direccion='a';
		x1 -= velMov;
		x2 -= velMov;
		if (!Warro.getWarro().bloqueando)
		{
			changeImage("caminopalotrolao.gif");
		}
		else
		{
			changeImage("gCamindandoGuardiaIzq.gif");
		}
		this.deattach(f);
	}
	/**
	 * Función que mueve al heroe a la derecha
	 * @param f El Field de juego declarado en el Main
	 */
	public void moveDer(Field f)
	{
		this.direccion='d';
		x1 += velMov;
		x2 += velMov;
		if (!Warro.getWarro().bloqueando)
		{
			changeImage("guerreroC.gif");
		}
		else
		{
			changeImage("gCamindandoGuardia.gif");
		}
		this.deattach(f);
	}
	/**
	 * Función que mueve al heroe arriva
	 * @param f El Field de juego declarado en el Main
	 */
	public void moveArr(Field f) 
	{
		y1 -= velMov;
		y2 -= velMov;
		if(this.direccion=='a') 
		{
			if (!Warro.getWarro().bloqueando)
			{
				this.changeImage("caminopalotrolao.gif");
			}
			else
			{
				this.changeImage("gCamindandoGuardiaIzq.gif");
			}
		}
		else 
		{
			if (!Warro.getWarro().bloqueando)
			{
				this.changeImage("guerreroC.gif");
			}
			else
			{
				this.changeImage("gCamindandoGuardia.gif");
			}
			
		}
		this.deattach(f);		
	}
	/**
	 * Función que mueve al heroe abajo
	 * @param f El Field de juego declarado en el Main
	 */
	public void moveAba(Field f) 
	{
		y1 += velMov;
		y2 += velMov;
		if(this.direccion=='a') 
		{
			if (!Warro.getWarro().bloqueando)
			{
				this.changeImage("caminopalotrolao.gif");
			}
			else
			{
				this.changeImage("gCamindandoGuardiaIzq.gif");
			}
		}
		else 
		{
			if (!Warro.getWarro().bloqueando)
			{
				this.changeImage("guerreroC.gif");
			}
			else
			{
				this.changeImage("gCamindandoGuardia.gif");
			}
		}
		//this.getGrounded(f);
		this.deattach(f);
	}
	/**
	 * Función que teletransporta al Heroe a la sala 1
	 * @param f El Field de juego declarado en el Main
	 * @throws IOException 
	 */
	public void teleport(Field f) throws IOException
	{
		if (this.collidesWith(Main.teleport)&& Main.sala6 == true) 
		{ 
			//Main.guardar();
			this.changeImage("teleport.gif");
			try 
			{
				Thread.sleep(1000);
			} catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.x1 = 874;
			this.x2 = 1034;
			this.y1 = 134;
			this.y2 = 294;
			Main.sala6 = false;
			f.background="sala1.png";
			Main.sala1 = true;
			Main.ronda ++;
			try 
			{
				Thread.sleep(1000);
			} catch (InterruptedException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
}
