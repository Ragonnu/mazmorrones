package juego;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * Clase que ejecuta el programa
 * @author Rafael González
 */
public class Main
{
	static Field f = new Field();
	public static Window w = new Window (f);
	/**
	 * Objeto que define la vida del heroe
	 */
	static Colisiones corazones = new Colisiones("vidaWarro.getWarro()", 100, 100, 350, 150, "3cor.png");
	/**
	 * Objetos que definen los cambios de sala
	 */
	static Colisiones s1D = new Colisiones("sala1D", 824, 750, 1166, 800, "");
	static Colisiones p1U = new Colisiones("pasillo1U", 920, 170, 950, 190, "");
	static Colisiones p1L = new Colisiones("pasillo1L", 368, 710, 682, 760, "");
	static Colisiones p1R = new Colisiones("pasillo1R", 1224, 655, 1528, 705, "");
	static Colisiones s3U = new Colisiones("sala3U", 888, 5, 1048, 15, "");
	static Colisiones s3D = new Colisiones("sala3D", 866, 880, 1100, 930, "");
	static Colisiones s4U = new Colisiones("sala4U", 866, 20, 1100, 70, "");
	static Colisiones s4D = new Colisiones("sala4D", 866, 880, 1100, 930, "");
	static Colisiones p2D = new Colisiones("pasillo2D", 804, 790, 1156, 840, "");
	static Colisiones p2L = new Colisiones("pasillo2L", 452, 275, 764, 325, "");
	static Colisiones p2R = new Colisiones("pasillo2R", 1316, 275, 1644, 325, "");
	static Colisiones s6U = new Colisiones("sala6U", 830, 5, 1070, 10, "");
	static Colisiones teleport = new Colisiones("teleportFinal", 876, 638, 1036, 798, "");
	static Colisiones speed = new Colisiones("speed", 874, 22, 1034, 82, "");
	/**
	 * Array que contiene las salas
	 */
	public static ArrayList<Mapa> salas = new ArrayList<Mapa>();
	
	/**
	 * Array que contiene los rankings
	 */
	static ArrayList<Ranking> ranking = new ArrayList<Ranking>();
	
	static Scanner sc = new Scanner(System.in);
	
	/**
	 * Boolean que determina el final del juego
	 */
	static boolean finPartida = false;
	static int cont;
	/**
	 * Variable que muestra en que sala estás
	 */
	static int sala = 1;
	/**
	 * Variable que muestra en que ronda estás
	 */
	static int ronda = 1;
	
	/**
	 * Booleans que asignan la sala actual
	 */
	static boolean sala1 = true;
	static boolean sala2 = false;
	static boolean sala3 = false;	
	static boolean sala4 = false;
	static boolean sala5 = false;	
	static boolean sala6 = false;
	static Mapa sala_actual = null;
	
	/**
	 * Objeto que muestra testo por pantalla
	 */
	static Texto t = new Texto("texto", 1550, 50, 1950, 140, "");
	static Texto r1 = new Texto("ranking", 870, 152, 114, 282, "");
	static Texto r2 = new Texto("ranking", 870, 312, 1174, 442, "");
	static Texto r3 = new Texto("ranking", 870, 472, 1174, 602, "");
	
	
	public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException 
	{
		cargarRanking();
		menu();
		salas.add(new Mapa("sala1"));
		salas.add(new Mapa("sala2"));
		salas.add(new Mapa("sala3"));
		salas.add(new Mapa("sala4"));
		salas.add(new Mapa("sala5"));
		salas.add(new Mapa("sala6"));
		w.changeSize(1925, 1000);
		f.background="sala1.png";
		
//		crearObstaculos();
//		crearEnemigos();
//		crearObjetos();
		jugar();
	}

/**
 * Función principal que genera todos los elementos del juego
 * @throws InterruptedException
 * @throws IOException
 * @throws ClassNotFoundException
 */
private static void jugar() throws InterruptedException, IOException, ClassNotFoundException 
{
	
	w.playMusic("m2.wav");
	crearObjetos();
	crearEnemigos();
	crearObstaculos();
	
	while (!finPartida)
	{		
		
		Warro.getWarro().update();
		input();
		dibujar();
		cambiadesala();
		Warro.getWarro().vida();
		Thread.sleep(20);
		System.out.println(sala);
		System.out.println(ronda);
//		System.out.println(Warro.getWarro().score);
	}
	guardarRanking();
}

/**
 * Función que carga los datos del ranking y los muestra por pantalla
 * @throws IOException
 */
private static void cargarRanking() throws IOException 
{
	File f = new File("ranking/RankingLeer.txt");
	FileReader in = new FileReader(f);
	BufferedReader br = new BufferedReader(in);
	
	while(br.ready()) 
	{
		String s = br.readLine();
		String[] spl = s.split(",");
		Ranking r = new Ranking("f",0);
		r.nombre = spl[0];
		r.puntos = Integer.parseInt(spl[1]);
		r.fecha = spl[2];
		ranking.add(r);
	}
	br.close();
}

	/**
	 * Función que guarda los puntos y el nombre de un jugador
	 * @throws IOException
	 */
	private static void guardarRanking() throws IOException
	{
		System.out.println("Escribe tu nombre");
		String nombre = w.showInputPopup("Escribre tu nombre");
		ranking.add(new Ranking(nombre,Warro.getWarro().score));
		
		File f2 = new File("ranking/RankingLeer.txt");
        FileWriter out = new FileWriter(f2);
        BufferedWriter bw = new BufferedWriter(out);
        f2.createNewFile();
        Collections.sort(ranking);
        for (Ranking ranking : ranking)
        {
            bw.write(ranking.nombre+","+ranking.puntos+","+ranking.fecha);
            bw.newLine();
        }
        bw.flush();
        bw.close();
	}

	
/**
 * Función que carga los objetos del juego en un Array
 */
private static void crearObjetos()
{
	Mapa m = new Mapa("");
	
	salas.get(3).objList.addAll(m.crearObjetos(4));
}
/**
 * Función que carga los enemigos del juego en un Array
 */
private static void crearEnemigos()
{
	
	Mapa m = new Mapa("");
	m.enemList.clear();
	salas.get(1).enemList.addAll(m.crearEnemigos(2));
	salas.get(2).enemList.addAll(m.crearEnemigos(3));
	salas.get(3).enemList.addAll(m.crearEnemigos(4));
	salas.get(5).enemList.addAll(m.crearEnemigos(6));
}
/**
 * Función que carga los obstaculos del juego en un Array
 */	
private static void crearObstaculos()
	{	
	Mapa m = new Mapa("");
	
	salas.get(0).obsList.addAll(m.crearObstaculos(1));
	salas.get(1).obsList.addAll(m.crearObstaculos(2));
	salas.get(2).obsList.addAll(m.crearObstaculos(3));
	salas.get(3).obsList.addAll(m.crearObstaculos(4));
	salas.get(4).obsList.addAll(m.crearObstaculos(5));
	salas.get(5).obsList.addAll(m.crearObstaculos(6));
	}

/**
 * Función que detecta los cambios de sala 
 */
public static void cambiadesala()
	{

		if (Warro.getWarro().collidesWith(speed)&&sala1 == true) 
		{
			Warro.getWarro().changeImage("poom.gif");
			Warro.getWarro().velMov = 10;
		}
		if (Warro.getWarro().collidesWith(s1D)&&sala1 == true) 
		{
			
			Warro.getWarro().x1 = 920;
			Warro.getWarro().x2 = 1080;
			Warro.getWarro().y1 = 254;
			Warro.getWarro().y2 = 414;
			sala1 = false;
			f.background="sala2.png";
			sala2 = true;
			sala = 2;
		
		}
		if (Warro.getWarro().collidesWith(p1U)&&sala2 == true) 
		{
			
			Warro.getWarro().x1 = 920;
			Warro.getWarro().x2 = 1080;
			Warro.getWarro().y1 = 550;
			Warro.getWarro().y2 = 710;
			sala2 = false;
			f.background="sala1.png";
			sala1 = true;
			sala = 1;
			
		}
		if (Warro.getWarro().collidesWith(p1L)&& sala2 == true) 
		{ 
			Warro.getWarro().x1 = 924;
			Warro.getWarro().x2 = 1084;
			Warro.getWarro().y1 = 86;
			Warro.getWarro().y2 = 246;
			sala2 = false;
			f.background="sala3.png";
			sala3 = true;
			sala = 3;
		
		}
		if (Warro.getWarro().collidesWith(p1R)&& sala2 == true) 
		{ 
			Warro.getWarro().x1 = 924;
			Warro.getWarro().x2 = 1084;
			Warro.getWarro().y1 = 86;
			Warro.getWarro().y2 = 246;
			sala2 = false;
			f.background="sala4.png";
			sala4 = true;
			sala = 4;
			
		}
		if (Warro.getWarro().collidesWith(s3U)&& sala3 == true) 
		{ 
			Warro.getWarro().x1 = 444;
			Warro.getWarro().x2 = 604;
			Warro.getWarro().y1 = 474;
			Warro.getWarro().y2 = 634;
			sala3 = false;
			f.background="sala2.png";
			sala2 = true;
			sala = 2;
			
		}
		if (Warro.getWarro().collidesWith(s3D)&& sala3 == true) 
		{ 
			Warro.getWarro().x1 = 516;
			Warro.getWarro().x2 = 676;
			Warro.getWarro().y1 = 338;
			Warro.getWarro().y2 = 498;
			sala3 = false;
			f.background="sala5.png";
			sala5 = true;
			sala = 5;
			
		}
		if (Warro.getWarro().collidesWith(s4U)&& sala4 == true) 
		{ 
			Warro.getWarro().x1 = 1322;
			Warro.getWarro().x2 = 1482;
			Warro.getWarro().y1 = 468;
			Warro.getWarro().y2 = 628;
			sala4 = false;
			f.background="sala2.png";
			sala2 = true;
			sala = 2;
		
		}
		if (Warro.getWarro().collidesWith(s4D)&& sala4 == true) 
		{ 
			Warro.getWarro().x1 = 1396;
			Warro.getWarro().x2 = 1556;
			Warro.getWarro().y1 = 338;
			Warro.getWarro().y2 = 498;
			sala4 = false;
			f.background="sala5.png";
			sala5 = true;
			sala = 5;
		
		}
		if (Warro.getWarro().collidesWith(p2R)&& sala5 == true) 
		{ 
			Warro.getWarro().x1 = 916;
			Warro.getWarro().x2 = 1076;
			Warro.getWarro().y1 = 686;
			Warro.getWarro().y2 = 846;
			sala5 = false;
			f.background="sala4.png";
			sala4 = true;
			sala = 4;
		
		}
		if (Warro.getWarro().collidesWith(p2L)&& sala5 == true) 
		{ 
			Warro.getWarro().x1 = 916;
			Warro.getWarro().x2 = 1074;
			Warro.getWarro().y1 = 686;
			Warro.getWarro().y2 = 846;
			sala5 = false;
			f.background="sala3.png";
			sala3 = true;
			sala = 3;
			
		}
		if (Warro.getWarro().collidesWith(p2D)&& sala5 == true) 
		{ 
			Warro.getWarro().x1 = 900;
			Warro.getWarro().x2 = 1060;
			Warro.getWarro().y1 = 50;
			Warro.getWarro().y2 = 210;
			sala5 = false;
			f.background="sala6.png";
			sala6 = true;
			sala = 6;
			
		}
		if (Warro.getWarro().collidesWith(s6U)&& sala6 == true) 
		{ 
			Warro.getWarro().x1 = 924;
			Warro.getWarro().x2 = 1084;
			Warro.getWarro().y1 = 554;
			Warro.getWarro().y2 = 714;
			sala6 = false;
			f.background="sala5.png";
			sala5 = true;
			sala = 5;
		
		}
	}

	/**
	 * Función que recoge todas las entradas por teclado y ejecuta las funciones correspondientes
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private static void input() throws InterruptedException, IOException, ClassNotFoundException
	{	
		
		if (w.getPressedKeys().contains('a') || w.getPressedKeys().contains('d') || w.getPressedKeys().contains('w')
				|| w.getPressedKeys().contains('s')|| w.getPressedKeys().contains('l')|| w.getPressedKeys().contains('h')
				|| w.getPressedKeys().contains('j')|| w.getPressedKeys().contains('k')|| w.getPressedKeys().contains('n')
				|| w.getPressedKeys().contains('t')|| w.getPressedKeys().contains('u')|| w.getPressedKeys().contains('m')) 
		{
			for(Enemigo e : sala_actual.enemList)
			{
				if (e instanceof Orco1)
				{
					((Orco1) e).eAtaca();
				}
				else if (e instanceof Orco2)
				{
					((Orco2) e).e2Ataca();
				}
			}

			if (w.getPressedKeys().contains('t')) 
			{
				Warro.getWarro().teleport(f);
				crearObjetos();
				crearEnemigos();

			}
			if (w.getPressedKeys().contains('h')) 
			{
				Warro.getWarro().estocada(f);
				
				Thread.sleep(1900);
			}
			if (w.getPressedKeys().contains('j')) 
			{
				Warro.getWarro().golpe(f);
				Thread.sleep(780);
			}
			if (w.getPressedKeys().contains('k')) 
			{
				Warro.getWarro().golpeRapido(f);
				Thread.sleep(600);
			}
			if (w.getPressedKeys().contains('u')) 
			{
				Warro.getWarro().golpeEspecial(f);
				Thread.sleep(600);
			}
			if (w.getPressedKeys().contains('l')) 
			{
				Warro.getWarro().golpeEscudo(f);
				Thread.sleep(400);
			}
			if (w.getPressedKeys().contains('n')) 
			{
				Warro.getWarro().cubrir(f);
				Warro.getWarro().bloqueando = true;
//				Thread.sleep(100);			
			}else {
				Warro.getWarro().bloqueando=false;
			}
			if (w.getPressedKeys().contains('a')) 
			{
				Warro.getWarro().moveIzq(f);
//				Warro.getWarro().iddle = true;

			}
			
			if (w.getPressedKeys().contains('d'))
			{
				Warro.getWarro().moveDer(f);

			}
			if (w.getPressedKeys().contains('w'))
			{
				
				Warro.getWarro().moveArr(f);

			}
			if (w.getPressedKeys().contains('s'))
			{
				Warro.getWarro().moveAba(f);

			}
			if (w.getPressedKeys().contains('m'))
			{
				w.playSFX("SongSave.wav");
				menuGuardar();
			}

		}

	}

	/**
	 * Función que guarda los datos de una partida
	 * @throws IOException
	 */
	public static void guardar() throws IOException 
	{
		File f = new File("guardado/guardar.txt");
		if(f.exists())f.delete();
		f.createNewFile();
		FileOutputStream fos = new FileOutputStream(f);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		
		oos.writeObject(Warro.getWarro());
		oos.writeObject(salas);
		oos.writeObject(corazones);
		oos.writeBoolean(sala1);
		oos.writeBoolean(sala2);
		oos.writeBoolean(sala3);
		oos.writeBoolean(sala4);
		oos.writeBoolean(sala5);
		oos.writeBoolean(sala6);
		
		
		oos.flush();
		oos.close();
	}
	/**
	 * Función que carga todos los datos de una partida ya guardada
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws InterruptedException
	 */
	private static void cargar() throws IOException, ClassNotFoundException, InterruptedException 
	{
		File f1 = new File("guardado/guardar.txt");
		if(f1.exists())
		{
			FileInputStream fis = new FileInputStream(f1);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Warro.w=(Warro) ois.readObject();
			salas=(ArrayList<Mapa>) ois.readObject();
			System.out.println(salas);
			corazones=(Colisiones) ois.readObject();
			sala1= ois.readBoolean();
			sala2= ois.readBoolean();
			sala3= ois.readBoolean();
			sala4= ois.readBoolean();
			sala5= ois.readBoolean();
			sala6= ois.readBoolean();
			
			if(sala1)
			{
				f.background="sala1.png";			
			}
			if(sala2)
			{
				f.background="sala2.png";			
			}
			if(sala3)
			{
				f.background="sala3.png";			
			}
			if(sala4)
			{
				f.background="sala4.png";			
			}
			if(sala5)
			{
				f.background="sala5.png";			
			}
			if(sala6)
			{
				f.background="sala6.png";			
			}
			salas.add(new Mapa("sala1"));
			salas.add(new Mapa("sala2"));
			salas.add(new Mapa("sala3"));
			salas.add(new Mapa("sala4"));
			salas.add(new Mapa("sala5"));
			salas.add(new Mapa("sala6"));
			jugar();
		}
	}

	/**
	 * Función que carga el menú principal ejecuta las acciones de este
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private static void menu() throws ClassNotFoundException, IOException, InterruptedException
	{
		w.playMusic("sonidoTaberna.wav");
		boolean jugar = false;
		while (!jugar) 
		{
			w.changeSize(1925, 1000);
			
			f.background = "fondoTaberna.gif";
			
			
			int x = f.getCurrentMouseX();
			int y = f.getCurrentMouseY();		
			ArrayList<Sprite> sp = new ArrayList<Sprite>();			
			Sprite iniciarPartida = new Sprite("Partida", 800, 252, 1104, 382, "botonJugar.png");
			Sprite continuarPartida = new Sprite("Continue",800, 412, 1104, 542,  "botonConti.png");
			Sprite ranking = new Sprite("RKG", 800, 572, 1104, 702, "botonRanking.png");
			sp.add(ranking);
			sp.add(iniciarPartida);
			sp.add(continuarPartida);
			f.draw(sp);
			for (Sprite s : sp) 
			{
				if(s.collidesWithPoint(x, y)) 
				{
					w.playSFX("clickBoton.wav");
					if (s.path == "botonJugar.png")
					{
						
						sala1 = true;
						sala2 = false;
						sala3 = false;
						sala4 = false;
						sala5 = false;
						sala6 = false;
						salas.add(new Mapa("sala1"));
						salas.add(new Mapa("sala2"));
						salas.add(new Mapa("sala3"));
						salas.add(new Mapa("sala4"));
						salas.add(new Mapa("sala5"));
						salas.add(new Mapa("sala6"));
						w.changeSize(1925, 1000);
						
//						crearObstaculos();
//						crearEnemigos();
//						crearObjetos();
						
						f.background="sala1.png";
						jugar();
						//jugar = true;
					
					}
					if (s.path == "botonRanking.png")
					{
						ranking();
					}
					if (s.path == "botonConti.png")
					{
						cargar();
					}
						
						
				}
			}
			try 
			{
				Thread.sleep(50);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
		
		
	}


	/**
	 * Función que muestra el Ranking
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private static void ranking() throws ClassNotFoundException, IOException, InterruptedException 
	{	
		w.playMusic("sonidoRanking.wav");
		boolean fin = false;
		while (!fin) 
		{			
			f.background = "fondoRanking.gif";
			int x = f.getCurrentMouseX();
			int y = f.getCurrentMouseY();
			Font fuente = new Font("URW Chancery L", Font.ROMAN_BASELINE, 34);			
			String puntos = ranking.get(0).nombre+": "+ranking.get(0).puntos;
			String puntos2 = ranking.get(1).nombre+": "+ranking.get(1).puntos;
			String puntos3= ranking.get(2).nombre+": "+ranking.get(2).puntos;
			r1.font=fuente;
			r2.font=fuente;
			r3.font=fuente;
			r1.path = puntos;
			r2.path = puntos2;
			r3.path = puntos3;
			int color = 0xFFFFFF;

			ArrayList<Sprite> sp = new ArrayList<Sprite>();
			Sprite salir = new Sprite("salir", 800, 850, 1104, 980, "botonVolver.png");
			sp.add(r1);
			sp.add(r2);
			sp.add(r3);
			sp.add(salir);
			f.draw(sp);
			if (salir.collidesWithPoint(x, y))
				
				{				
					if (salir.path == "botonVolver.png") 
					{
						w.playSFX("clickBoton.wav");
						menu();
						fin = true;
					}
				}			
			try 
			{
				Thread.sleep(50);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			} 
		}		
	}
	/**
	 * Función que muestra el menú Guardar y ejecuta las acciones de este
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private static void menuGuardar() throws ClassNotFoundException, IOException, InterruptedException
	{
		boolean jugar = false;
		Sprite guardar = new Sprite("Guardar", 800, 252, 1104, 382, "botonGuardar.png");
		Sprite continuar = new Sprite("Continue",800, 412, 1104, 542,  "botonConti.png");
		Sprite salir = new Sprite("Salir", 800, 572, 1104, 702, "botonSalir.png");
		while (!jugar) 
		{
			w.changeSize(1925, 1000);
			
			f.background = "MenuGuardar.jpg";
			
			
			int x = f.getCurrentMouseX();
			int y = f.getCurrentMouseY();		
			ArrayList<Sprite> sp = new ArrayList<Sprite>();
			sp.add(guardar);
			sp.add(salir);
			sp.add(continuar);
			f.draw(sp);
			for (Sprite s : sp) 
			{
				if(s.collidesWithPoint(x, y)) 
				{
					w.playSFX("clickBoton.wav");
					if (s.path == "botonGuardar.png")
					{
						guardar();						
						guardar.path = "botonHecho.png";
					}
					if (s.path == "botonSalir.png")
					{
						finPartida = true;
						System.out.println(finPartida);
					}
					if (s.path == "botonConti.png")
					{
						cargar();
					}
						
						
				}
			}
			try 
			{
				Thread.sleep(50);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
		
		
	}
	/**
	 * Función que carga en una array todo lo que se ha de mostrar en el juego
	 */
	private static void dibujar()
	{
		Font fuente = new Font("URW Chancery L", Font.ROMAN_BASELINE, 34);		
		String puntos = "Score: "+Warro.getWarro().score + "";
		t.font=fuente;
		t.path = puntos;
		int color = 0xFFFFFF;
		t.textColor=color;
		ArrayList<Sprite> sp = new ArrayList<Sprite>();
		sp.add(t);
		sp.add(Warro.getWarro());
		sp.add(corazones);
		if (sala1)
		{
			sala_actual = salas.get(0);
			sp.addAll(salas.get(0).obsList);
		}
		else if (sala2)
		{
			sala_actual = salas.get(1);
			sp.addAll(salas.get(1).obsList);
			sp.addAll(salas.get(1).enemList);
		}
		else if (sala3)
		{
			sala_actual = salas.get(2);
			sp.addAll(salas.get(2).obsList);
			sp.addAll(salas.get(2).enemList);
		}
		else if (sala4)
		{
			sala_actual = salas.get(3);
			sp.addAll(salas.get(3).obsList);
			sp.addAll(salas.get(3).enemList);
			sp.addAll(salas.get(3).objList);
		}
		else if (sala5)
		{
			sala_actual = salas.get(4);
			sp.addAll(salas.get(4).obsList);
		}
		else if (sala6)
		{
			sala_actual = salas.get(5);
			sp.addAll(salas.get(5).obsList);
			sp.addAll(salas.get(5).enemList);
		}
		f.draw(sp);
	}

	/**
	 *Función que borra la array salas 
	 */
	public static void borrarSalas() 
	{
		salas.clear();		
	}
}
