package juego;


/**
 * Clase que define todas las funciones específicas de la clase Obstaculo
 * @author Rafael González
 *
 */
public class Obstaculo extends Sprite
{
	public Obstaculo(String name, int x1, int y1, int x2, int y2, String path)
	{
		super(name, x1, y1, x2, y2, path);
		this.terrain=true;
	}
	
	
}
